<?php
// https://gitee.com/xiangzai290
header('Content-type:text');
define("Token", "test"); //后面的可以修改
if (!isset($_GET['echostr'])) {
    responseMsg();
}else{
    valid();
}
function valid()
{
    $echoStr = $_GET["echostr"];
    $signature = $_GET["signature"];
    $timestamp = $_GET["timestamp"];
    $nonce = $_GET["nonce"];
    $token = Token;
    $tmpArr = array($token, $timestamp, $nonce);
    sort($tmpArr, SORT_STRING);
    $tmpStr = sha1(implode($tmpArr));
    if($tmpStr == $signature){
        echo $echoStr;
        exit;
    }
}
function responseMsg()
{
    $postStr = file_get_contents("php://input");
    if (!empty($postStr)){
        $object = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlTpl = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[%s]]></Content></xml>";
        $result = sprintf($xmlTpl, $object->FromUserName, $object->ToUserName, time(), $object->FromUserName);
        echo $result;
    }else{
        exit;
    }
}
?>
